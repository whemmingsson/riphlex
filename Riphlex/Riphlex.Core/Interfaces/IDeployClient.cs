﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core.Interfaces
{
    public interface IDeployClient
    {
        void Deploy(string fromPath, string toPath);
    }
}
