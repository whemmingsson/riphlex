﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core.Interfaces
{
    public interface IDefinitionStorage
    {
        void Add(DeployDefinition definition);
        IEnumerable<DeployDefinition> GetAll();
        void Add(List<DeployDefinition> _definitions);
    }
}
