﻿using System.Collections.Generic;

namespace Riphlex.Core.Interfaces
{
    public interface IDefinitionClient
    {
        void AddDefinition(DeployDefinition definition);
        DeployDefinition GetDefinition(string name);
        ICollection<DeployDefinition> GetDefinitions();
        void UpdateDefinition(DeployDefinition definition);
    }
}