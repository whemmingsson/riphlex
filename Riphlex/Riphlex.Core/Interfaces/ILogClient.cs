﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core.Interfaces
{
    public interface ILogClient
    {
        void UpdateRecord(string target);
        void UpdateRecordWithSource(string source);
        void Log();
    }
}
