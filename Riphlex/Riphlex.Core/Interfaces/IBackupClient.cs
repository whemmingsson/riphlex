﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core.Interfaces
{
    public interface IBackupClient
    {
        void Backup(string fromPath, string toPath);
    }
}
