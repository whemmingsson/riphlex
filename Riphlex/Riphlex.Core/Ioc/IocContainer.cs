﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Riphlex.Core.Ioc
{
    public class IocContainer
    {
        private static IocContainer _current;

        private Dictionary<Type, ICollection<Implementation>> _container;

        private IocContainer()
        {
            _container = new Dictionary<Type, ICollection<Implementation>>();
        }

        public static IocContainer Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new IocContainer();
                }

                return _current;
            }
        }

        public void Register<TInterface, TConcreteType>() where TConcreteType : class, TInterface, new()
        {
            Register<TInterface, TConcreteType>(false, null);
        }

        public void Register<TInterface, TConcreteType>(string label) where TConcreteType : class, TInterface, new()
        {
            Register<TInterface, TConcreteType>(false, label);
        }

        public void RegisterSingleton<TInterface, TConcreteType>() where TConcreteType : class, TInterface, new()
        {
            Register<TInterface, TConcreteType>(true, null);
        }

        public TInterface GetInstance<TInterface>()
        {
            if (!_container.ContainsKey(typeof(TInterface)))
            {
                throw new Exception("The provided interface has not been registred");
            }

            return CreateInstanceOrGetSingleton<TInterface>(_container[typeof(TInterface)].First());
        }

        public TInterface GetInstance<TInterface>(string label)
        {
            if (!_container.ContainsKey(typeof(TInterface)))
            {
                throw new Exception("The provided interface has not been registred");
            }

            var firstImplementation = _container[typeof(TInterface)]
                .FirstOrDefault(implementation => implementation.Label.Equals(label));

            if (firstImplementation == default(Implementation))
            {
                throw new Exception("No implementation with the provided label exists");
            }

            return CreateInstanceOrGetSingleton<TInterface>(firstImplementation);
        }

        private TInterface CreateInstanceOrGetSingleton<TInterface>(Implementation firstImplementation)
        {
            if (!firstImplementation.IsSingleton)
            {
                return CreateInstance<TInterface>(firstImplementation.Type);
            }

            if (firstImplementation.Instance == null)
            {
                firstImplementation.Instance = CreateInstance<TInterface>(firstImplementation.Type);
            }

            return (TInterface)firstImplementation.Instance;
        }

        private void Register<TInterface, TConcreteType>(bool isSingleton, string label)
        {
            if (_container.ContainsKey(typeof(TInterface)) && label == null)
            {
                throw new Exception("The provided interface has already been registered");
            }

            var implementation = new Implementation(typeof(TConcreteType), isSingleton, label);

            if (_container.ContainsKey(typeof(TInterface)) && label != null)
            {
                _container[typeof(TInterface)].Add(implementation);
            }
            else
            {
                _container.Add(typeof(TInterface),
               new[]
               {
                    implementation
               });
            }         
        }

        private TInterface CreateInstance<TInterface>(Type concreteType)
        {
            return (TInterface)Activator.CreateInstance(concreteType);
        }

        private class Implementation
        {
            public Implementation(Type type, bool isSingleton, string label)
            {
                Type = type;
                IsSingleton = isSingleton;
                Label = label;
            }

            public Type Type { set; get; }
            public object Instance { set; get; }
            public bool IsSingleton { set; get; }
            public string Label { set; get; }
        }
    }
}
