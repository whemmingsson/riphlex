﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Riphlex.Core.Helpers
{
    public static class DirectoryHelper
    {
        public static IEnumerable<string> GetAllFiles(string path)
        {
            var result = new List<string>();

            result.AddRange(Directory.GetFiles(path));

            foreach (var directory in Directory.GetDirectories(path))
            {
                result.AddRange(GetAllFiles(directory));
            }

            return result;
        }

        public static string GetDirectoryPath(string completePath)
        {
            var directories = completePath.Split("\\".ToCharArray());

            var result = "";
            foreach (var directory in directories.Take(directories.Count() - 1))
            {
                result += directory + "\\";
            }

            return result;
        }

        public static void CreateDirectoryIfMissing(string toFilePath)
        {
            var directoryPath = GetDirectoryPath(toFilePath);

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        public static string CreateToFilePath(string filePath, string fromPath, string toPath)
        {
            return toPath + filePath.Remove(0, fromPath.Length);
        }
    }
}
