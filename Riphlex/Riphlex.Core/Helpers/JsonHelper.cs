﻿using Newtonsoft.Json;
using System.IO;

namespace Riphlex.Core.Helpers
{
    public class JsonHelper<T> where T : class, new()
    {
        private string _storagePath;

        public JsonHelper(string storagePath)
        {
            _storagePath = storagePath;
        }

        public string StoragePath
        {
            set { _storagePath = value; }
        }

        public JsonHelper()
        {
                        
        }

        public void SerializeToJson(T data)
        {
            CheckIfStoragePathExists();

            var json = JsonConvert.SerializeObject(data);
            File.WriteAllText(_storagePath, json);
        }

        public T DeserializeFromJson()
        {
            CheckIfStoragePathExists();

            if (File.Exists(_storagePath))
            {
                return JsonConvert.DeserializeObject<T>(File.ReadAllText(_storagePath));
            }

            return null;
        }

        private void CheckIfStoragePathExists()
        {
            if(_storagePath == null)
            {
                throw new System.Exception("Unable to access storage without the StoragePath set");
            }
        }
    }
}
