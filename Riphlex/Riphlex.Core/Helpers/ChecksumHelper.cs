﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Riphlex.Core.Helpers
{
    public static class ChecksumHelper
    {
        public static string CreateMD5Checksum(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }
        }
    }
}
