﻿using Riphlex.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Riphlex.Core.Helpers;

namespace Riphlex.Core.DataAccess
{
    public class DefinitionStorage : IDefinitionStorage
    {
        private JsonHelper<List<DeployDefinition>> _jsonHelper;
        private const string Filepath = "definitions.json";

        public DefinitionStorage()
        {
            _jsonHelper = new JsonHelper<List<DeployDefinition>>(Filepath);
        }

        public void Add(DeployDefinition definition)
        {
            var definitions = new List<DeployDefinition>();
            var definitionsFromFile = GetAll();

            if(definitionsFromFile != null)
            {
                definitions = definitionsFromFile.ToList();
            }

            definitions.Add(definition);
            _jsonHelper.SerializeToJson(definitions);
        }

        public IEnumerable<DeployDefinition> GetAll()
        {
            return _jsonHelper.DeserializeFromJson();
        }

        public void Add(List<DeployDefinition> definitions)
        {
            _jsonHelper.SerializeToJson(definitions);
        }
    }
}
