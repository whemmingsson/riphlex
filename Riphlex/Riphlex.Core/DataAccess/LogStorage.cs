﻿using Riphlex.Core.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Riphlex.Core.DataAccess
{
    public class LogStorage
    {
        private JsonHelper<List<LogEntry>> _jsonHelper;
        private string _logPath = "syslog.log";

        public LogStorage()
        {
            _jsonHelper = new JsonHelper<List<LogEntry>>(_logPath);
        }

        public void Add(LogEntry logEntry)
        {
            var entries = new List<LogEntry>();
            var entriesFromFile = GetAll();

            if (entriesFromFile != null)
            {
                entries = entriesFromFile.ToList();
            }

            entries.Add(logEntry);
            _jsonHelper.SerializeToJson(entries);
        }

        public IEnumerable<LogEntry> GetAll()
        {
            return _jsonHelper.DeserializeFromJson();
        }
    }
}
