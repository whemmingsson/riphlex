﻿using Riphlex.Core.Interfaces;
using Riphlex.Core.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Riphlex.Core
{
    public class DefinitionClient : IDefinitionClient
    {
        private ICollection<DeployDefinition> _definitions;

        private IDefinitionStorage _store;

        public DefinitionClient()
        {
            _definitions = new List<DeployDefinition>();
            _store = IocContainer.Current.GetInstance<IDefinitionStorage>();
            LoadDefinitionsFromStore();
        }

        private void LoadDefinitionsFromStore()
        {
            var definitions = _store.GetAll();

            if (definitions != null)
            {
                _definitions = definitions.ToList();
            }
        }

        public void AddDefinition(DeployDefinition definition)
        {
            if (!_definitions.Any(d => d.Name.Equals(definition.Name)))
            {
                _definitions.Add(definition);
                _store.Add(definition);
            }
            else
            {
                throw new Exception("Cannot add - definition with name already exists");
            }
        }

        public DeployDefinition GetDefinition(string name)
        {
            return _definitions.FirstOrDefault(d => d.Name.Equals(name));
        }

        public ICollection<DeployDefinition> GetDefinitions()
        {
            return _definitions;
        }

        public void UpdateDefinition(DeployDefinition definition)
        {
            var definitionToUpdate = GetDefinition(definition.Name);

            definitionToUpdate.BackupPath = definition.BackupPath;
            definitionToUpdate.SourcePath = definition.SourcePath;
            definitionToUpdate.TargetPaths = definition.TargetPaths;

            _store.Add(_definitions.ToList());
        }
    }
}
