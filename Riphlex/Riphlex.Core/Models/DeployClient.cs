﻿using Riphlex.Core.Helpers;
using Riphlex.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core
{
    public class DeployClient : IDeployClient
    {
        private const string BackupFileName = "myZipBackup.zip"; // TODO: Resolve this name from the definition instead?

        public DeployClient() { }   

        public void Deploy(string fromPath, string toPath)
        {
            foreach(var file in DirectoryHelper.GetAllFiles(fromPath))
            {
                var toFilePath = DirectoryHelper.CreateToFilePath(file, fromPath, toPath);
                DirectoryHelper.CreateDirectoryIfMissing(toFilePath);
                File.Copy(file, toFilePath, true);
            }
        }       
    }
}
