﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core
{
    /// <summary>
    /// Replaces the RecordEntry
    /// </summary>
    public class Entry
    {
        public FileInfo SourceFile { set; get; }

        public List<FileInfo> TargetFiles { set; get; }

        public Entry()
        {

        }

        public Entry(FileInfo sourceFile)
        {
            SourceFile = sourceFile;
        }
    }
}
