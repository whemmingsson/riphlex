﻿using Riphlex.Core.DataAccess;
using Riphlex.Core.Helpers;
using Riphlex.Core.Interfaces;
using System;

namespace Riphlex.Core
{
    public class FileLogClient /*: ILogClient*/
    {
        private LogStorage _storage;

        private LogEntry _inMemoryEntry;

        public FileLogClient()
        {
            _storage = new LogStorage();
            _inMemoryEntry = new LogEntry();
        }

        /// <summary>
        /// Create a log record
        /// </summary>
        /// <param name="targetFolder">The folder where the deploy will be performed</param>
        /// <param name="sourceFolder">The source folder, containing new files to deploy</param>
        public void CreateRecord(string sourceFolder)
        {
           // var filesInFolder = DirectoryHelper.GetAllFiles(target);
          //  _record.AddRecord(target, filesInFolder);
        }

        public void UpdateRecordWithSource(string source)
        {
            //var filesInFolder = DirectoryHelper.GetAllFiles(source);
            //_record.AddSourceRecord(source, filesInFolder);
        }

        /// <summary>
        /// Save the current in-memory log to storage
        /// </summary>
        public void SaveLog()
        {
            _storage.Add(_inMemoryEntry);
        }

        [Obsolete("Method not used any more")]
        public void Log()
        {
           //  CONTENT OF THIS METHOD WAS REMOVED 2016-08-22
        }
    }
}
