﻿namespace Riphlex.Core
{
    public class FileInfo
    {
        public string Path { get; set; }
        public string Checksum { get; set; }

        public FileInfo() {}

        public FileInfo(string path, string checksum)
        {
            Path = path;
            Checksum = checksum;
        }
    }
}
