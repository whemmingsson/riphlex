﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riphlex.Core
{
    public class DeployDefinition
    {
        private Guid _uniqueIdentifier;

        public string Name { get; set; }
        public string BackupPath { get; set; }
        public string SourcePath { get; set; }
        public ICollection<string> TargetPaths { get; set; }

        public DeployDefinition()
        {
            TargetPaths = new List<string>();
            _uniqueIdentifier = Guid.NewGuid();
        }
    }
}
