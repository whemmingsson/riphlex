﻿using Riphlex.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using System.IO;

namespace Riphlex.Core
{
    public class ZipBackupClient : IBackupClient
    {
        private const string BackupFileName = "myZipBackup.zip";

        public ZipBackupClient(){}

        public void Backup(string fromPath, string toPath)
        {        
            var backupFile = toPath + "/" + BackupFileName;

            using (var zip = new ZipFile())
            {
                zip.AddDirectory(fromPath);
                zip.Save(backupFile);
            }
        }
    }
}
