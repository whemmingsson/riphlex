﻿using Riphlex.Core.Helpers;
using System.Collections.Generic;

namespace Riphlex.Core
{
    public class LogRecord
    {
        private Dictionary<string, IEnumerable<RecordEntry>> _targetFiles;

        private string _sourcePath;
        private IEnumerable<RecordEntry> _sourceFiles;

        public LogRecord()
        {
            _targetFiles = new Dictionary<string, IEnumerable<RecordEntry>>();
            _sourceFiles = new List<RecordEntry>();
        }

        public Dictionary<string, IEnumerable<RecordEntry>> AffectedFiles
        {
            get { return _targetFiles; }
        }

        public IEnumerable<RecordEntry> SourceFiles
        {
            get { return _sourceFiles; }
        }
        
        public string SourcePath
        {
            get
            {
                return _sourcePath;
            }
        }

        public void AddSourceRecord(string sourcePath, IEnumerable<string> files)
        {
            _sourcePath = sourcePath;
            _sourceFiles = GetRecordEntries(sourcePath, files);
        }

        public void AddRecord(string targetPath, IEnumerable<string> files)
        {
            _targetFiles.Add(targetPath, GetRecordEntries(targetPath, files));
        }

        private static IEnumerable<RecordEntry> GetRecordEntries(string path, IEnumerable<string> files)
        {
            foreach (var file in files)
            {
                yield return new RecordEntry()
                {
                    //Path = file.Replace(path, ""),
                    //Checksum = ChecksumHelper.CreateMD5Checksum(file)
                };
            }
        }
    }
}
