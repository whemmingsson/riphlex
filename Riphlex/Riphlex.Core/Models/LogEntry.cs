﻿using System;
using System.Collections.Generic;

namespace Riphlex.Core
{
    /// <summary>
    /// Class that can be serialized to JSON
    /// </summary>
    public class LogEntry
    {
        public Guid Guid { private set; get;}

        /// <summary>
        /// Date and time for the deployment
        /// </summary>
        public DateTime Date { set; get; }
        /// <summary>
        /// The definition used in the deploy
        /// </summary>
        public DeployDefinition Definition { set; get; }

        public List<Entry> AllFiles { set; get; }

        public LogEntry()
        {
            Guid = Guid.NewGuid();
            AllFiles = new List<Entry>();
        }
    }
}
