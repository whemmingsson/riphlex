﻿namespace Riphlex.Core
{
    public class RecordEntry
    {
        public bool Changed { set; get; }
        public FileInfo Source { set; get; }
        public FileInfo Target { set; get; }

        public RecordEntry() {}
 
        public RecordEntry(FileInfo source, FileInfo target)
        {
            Source = source;
            Target = target;
        }
    }
}
