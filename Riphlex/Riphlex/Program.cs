﻿using Riphlex.Core;
using Riphlex.Core.DataAccess;
using Riphlex.Core.Interfaces;
using Riphlex.Core.Ioc;
using System;
using System.Windows.Forms;

namespace Riphlex
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            RegisterImplementations();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RiphlexForm());
        }

        private static void RegisterImplementations()
        {
            var container = IocContainer.Current;

            container.Register<IBackupClient, ZipBackupClient>();
            container.Register<IDeployClient, DeployClient>();
            container.Register<IDefinitionStorage, DefinitionStorage>();
            container.Register<IDefinitionClient, DefinitionClient>();
            //container.Register<ILogClient, FileLogClient>();
        }
    }
}
