﻿namespace Riphlex
{
    partial class RiphlexForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxDefinitions = new System.Windows.Forms.ListBox();
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.tabRollback = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listPreviousDeploys = new System.Windows.Forms.ListBox();
            this.tabUserLogs = new System.Windows.Forms.TabPage();
            this.settingsTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSelectSourceFolder = new System.Windows.Forms.Button();
            this.btnSelectBackupFolder = new System.Windows.Forms.Button();
            this.btnSelectTargetFolders = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.listBoxTargets = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBackup = new System.Windows.Forms.TextBox();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.btnSaveDefinition = new System.Windows.Forms.Button();
            this.btnCreateNewDefinition = new System.Windows.Forms.Button();
            this.mainTab = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBackupAndDeploy = new System.Windows.Forms.Button();
            this.allTabs = new System.Windows.Forms.TabControl();
            this.button2 = new System.Windows.Forms.Button();
            this.gb1.SuspendLayout();
            this.tabRollback.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.settingsTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.mainTab.SuspendLayout();
            this.allTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxDefinitions
            // 
            this.listBoxDefinitions.FormattingEnabled = true;
            this.listBoxDefinitions.Location = new System.Drawing.Point(6, 22);
            this.listBoxDefinitions.Name = "listBoxDefinitions";
            this.listBoxDefinitions.Size = new System.Drawing.Size(149, 394);
            this.listBoxDefinitions.TabIndex = 0;
            this.listBoxDefinitions.SelectedIndexChanged += new System.EventHandler(this.listBoxDefinitions_SelectedIndexChanged);
            this.listBoxDefinitions.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxDefinitions_MouseDoubleClick);
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.listBoxDefinitions);
            this.gb1.Location = new System.Drawing.Point(416, 12);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(161, 421);
            this.gb1.TabIndex = 1;
            this.gb1.TabStop = false;
            this.gb1.Text = "Avaliable definitions";
            // 
            // tabRollback
            // 
            this.tabRollback.Controls.Add(this.label6);
            this.tabRollback.Controls.Add(this.button1);
            this.tabRollback.Controls.Add(this.groupBox2);
            this.tabRollback.Location = new System.Drawing.Point(4, 22);
            this.tabRollback.Name = "tabRollback";
            this.tabRollback.Padding = new System.Windows.Forms.Padding(3);
            this.tabRollback.Size = new System.Drawing.Size(385, 395);
            this.tabRollback.TabIndex = 3;
            this.tabRollback.Text = "Rollback";
            this.tabRollback.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Select an existing definition to view all deploys";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 347);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Rollback deploy";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listPreviousDeploys);
            this.groupBox2.Location = new System.Drawing.Point(15, 62);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox2.Size = new System.Drawing.Size(364, 270);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Previous deploys";
            // 
            // listPreviousDeploys
            // 
            this.listPreviousDeploys.FormattingEnabled = true;
            this.listPreviousDeploys.Location = new System.Drawing.Point(6, 19);
            this.listPreviousDeploys.Name = "listPreviousDeploys";
            this.listPreviousDeploys.Size = new System.Drawing.Size(352, 238);
            this.listPreviousDeploys.TabIndex = 0;
            // 
            // tabUserLogs
            // 
            this.tabUserLogs.Location = new System.Drawing.Point(4, 22);
            this.tabUserLogs.Name = "tabUserLogs";
            this.tabUserLogs.Padding = new System.Windows.Forms.Padding(3);
            this.tabUserLogs.Size = new System.Drawing.Size(385, 395);
            this.tabUserLogs.TabIndex = 2;
            this.tabUserLogs.Text = "User logs";
            this.tabUserLogs.UseVisualStyleBackColor = true;
            // 
            // settingsTab
            // 
            this.settingsTab.Controls.Add(this.groupBox1);
            this.settingsTab.Location = new System.Drawing.Point(4, 22);
            this.settingsTab.Name = "settingsTab";
            this.settingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.settingsTab.Size = new System.Drawing.Size(385, 395);
            this.settingsTab.TabIndex = 1;
            this.settingsTab.Text = "Settings ";
            this.settingsTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnSelectSourceFolder);
            this.groupBox1.Controls.Add(this.btnSelectBackupFolder);
            this.groupBox1.Controls.Add(this.btnSelectTargetFolders);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.listBoxTargets);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBackup);
            this.groupBox1.Controls.Add(this.txtSource);
            this.groupBox1.Controls.Add(this.btnSaveDefinition);
            this.groupBox1.Controls.Add(this.btnCreateNewDefinition);
            this.groupBox1.Location = new System.Drawing.Point(15, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 360);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create/edit definition";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(20, 49);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(141, 20);
            this.txtName.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Name";
            // 
            // btnSelectSourceFolder
            // 
            this.btnSelectSourceFolder.Location = new System.Drawing.Point(292, 101);
            this.btnSelectSourceFolder.Name = "btnSelectSourceFolder";
            this.btnSelectSourceFolder.Size = new System.Drawing.Size(35, 23);
            this.btnSelectSourceFolder.TabIndex = 7;
            this.btnSelectSourceFolder.Text = "...";
            this.btnSelectSourceFolder.UseVisualStyleBackColor = true;
            this.btnSelectSourceFolder.Click += new System.EventHandler(this.btnSelectSourceFolder_Click);
            // 
            // btnSelectBackupFolder
            // 
            this.btnSelectBackupFolder.Location = new System.Drawing.Point(292, 151);
            this.btnSelectBackupFolder.Name = "btnSelectBackupFolder";
            this.btnSelectBackupFolder.Size = new System.Drawing.Size(35, 23);
            this.btnSelectBackupFolder.TabIndex = 7;
            this.btnSelectBackupFolder.Text = "...";
            this.btnSelectBackupFolder.UseVisualStyleBackColor = true;
            this.btnSelectBackupFolder.Click += new System.EventHandler(this.btnSelectBackupFolder_Click);
            // 
            // btnSelectTargetFolders
            // 
            this.btnSelectTargetFolders.Location = new System.Drawing.Point(292, 252);
            this.btnSelectTargetFolders.Name = "btnSelectTargetFolders";
            this.btnSelectTargetFolders.Size = new System.Drawing.Size(35, 23);
            this.btnSelectTargetFolders.TabIndex = 7;
            this.btnSelectTargetFolders.Text = "...";
            this.btnSelectTargetFolders.UseVisualStyleBackColor = true;
            this.btnSelectTargetFolders.Click += new System.EventHandler(this.btnSelectTargetFolders_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Target folder(s)";
            // 
            // listBoxTargets
            // 
            this.listBoxTargets.FormattingEnabled = true;
            this.listBoxTargets.Location = new System.Drawing.Point(20, 206);
            this.listBoxTargets.Name = "listBoxTargets";
            this.listBoxTargets.Size = new System.Drawing.Size(266, 69);
            this.listBoxTargets.TabIndex = 5;
            this.listBoxTargets.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listBoxTargets_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Backup folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Source folder";
            // 
            // txtBackup
            // 
            this.txtBackup.BackColor = System.Drawing.SystemColors.Window;
            this.txtBackup.Location = new System.Drawing.Point(20, 153);
            this.txtBackup.Name = "txtBackup";
            this.txtBackup.Size = new System.Drawing.Size(266, 20);
            this.txtBackup.TabIndex = 3;
            // 
            // txtSource
            // 
            this.txtSource.BackColor = System.Drawing.SystemColors.Window;
            this.txtSource.Location = new System.Drawing.Point(20, 101);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(266, 20);
            this.txtSource.TabIndex = 3;
            // 
            // btnSaveDefinition
            // 
            this.btnSaveDefinition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveDefinition.Location = new System.Drawing.Point(20, 292);
            this.btnSaveDefinition.Name = "btnSaveDefinition";
            this.btnSaveDefinition.Size = new System.Drawing.Size(100, 23);
            this.btnSaveDefinition.TabIndex = 2;
            this.btnSaveDefinition.Text = "Save";
            this.btnSaveDefinition.UseVisualStyleBackColor = true;
            this.btnSaveDefinition.Click += new System.EventHandler(this.btnSaveDefinition_Click);
            // 
            // btnCreateNewDefinition
            // 
            this.btnCreateNewDefinition.Location = new System.Drawing.Point(126, 292);
            this.btnCreateNewDefinition.Name = "btnCreateNewDefinition";
            this.btnCreateNewDefinition.Size = new System.Drawing.Size(100, 23);
            this.btnCreateNewDefinition.TabIndex = 2;
            this.btnCreateNewDefinition.Text = "Create new";
            this.btnCreateNewDefinition.UseVisualStyleBackColor = true;
            this.btnCreateNewDefinition.Click += new System.EventHandler(this.btnCreateNewDefinition_Click);
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.button2);
            this.mainTab.Controls.Add(this.label1);
            this.mainTab.Controls.Add(this.btnBackupAndDeploy);
            this.mainTab.Location = new System.Drawing.Point(4, 22);
            this.mainTab.Name = "mainTab";
            this.mainTab.Padding = new System.Windows.Forms.Padding(3);
            this.mainTab.Size = new System.Drawing.Size(385, 395);
            this.mainTab.TabIndex = 0;
            this.mainTab.Text = "Safe deploy";
            this.mainTab.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select a definition and press the button";
            // 
            // btnBackupAndDeploy
            // 
            this.btnBackupAndDeploy.Location = new System.Drawing.Point(99, 166);
            this.btnBackupAndDeploy.Name = "btnBackupAndDeploy";
            this.btnBackupAndDeploy.Size = new System.Drawing.Size(150, 23);
            this.btnBackupAndDeploy.TabIndex = 0;
            this.btnBackupAndDeploy.Text = "Backup and deploy";
            this.btnBackupAndDeploy.UseVisualStyleBackColor = true;
            this.btnBackupAndDeploy.Click += new System.EventHandler(this.btnBackupAndDeploy_Click);
            // 
            // allTabs
            // 
            this.allTabs.Controls.Add(this.mainTab);
            this.allTabs.Controls.Add(this.tabRollback);
            this.allTabs.Controls.Add(this.settingsTab);
            this.allTabs.Controls.Add(this.tabUserLogs);
            this.allTabs.Location = new System.Drawing.Point(12, 12);
            this.allTabs.Name = "allTabs";
            this.allTabs.SelectedIndex = 0;
            this.allTabs.Size = new System.Drawing.Size(393, 421);
            this.allTabs.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(343, 366);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(36, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "[log]";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RiphlexForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 453);
            this.Controls.Add(this.gb1);
            this.Controls.Add(this.allTabs);
            this.Name = "RiphlexForm";
            this.Text = "Riphlex Safe Deploy Alfa";
            this.gb1.ResumeLayout(false);
            this.tabRollback.ResumeLayout(false);
            this.tabRollback.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.settingsTab.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.mainTab.ResumeLayout(false);
            this.mainTab.PerformLayout();
            this.allTabs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.ListBox listBoxDefinitions;
        private System.Windows.Forms.TabPage tabRollback;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listPreviousDeploys;
        private System.Windows.Forms.TabPage tabUserLogs;
        private System.Windows.Forms.TabPage settingsTab;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSelectSourceFolder;
        private System.Windows.Forms.Button btnSelectBackupFolder;
        private System.Windows.Forms.Button btnSelectTargetFolders;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBoxTargets;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBackup;
        private System.Windows.Forms.TextBox txtSource;
        private System.Windows.Forms.Button btnSaveDefinition;
        private System.Windows.Forms.Button btnCreateNewDefinition;
        private System.Windows.Forms.TabPage mainTab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBackupAndDeploy;
        private System.Windows.Forms.TabControl allTabs;
        private System.Windows.Forms.Button button2;
    }
}

