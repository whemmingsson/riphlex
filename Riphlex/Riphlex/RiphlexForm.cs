﻿using Riphlex.Core;
using Riphlex.Core.Interfaces;
using Riphlex.Core.Ioc;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Riphlex
{
    public partial class RiphlexForm : Form
    {
        private IDeployClient _deployClient;
        private IBackupClient _backupClient;
        private IDefinitionClient _definitionClient;
        private ILogClient _logClient;

        public RiphlexForm()
        {
            InitializeComponent();

            _deployClient = IocContainer.Current.GetInstance<IDeployClient>();
            _backupClient = IocContainer.Current.GetInstance<IBackupClient>();
            _definitionClient = IocContainer.Current.GetInstance<IDefinitionClient>();
            _logClient = IocContainer.Current.GetInstance<ILogClient>();

            SetupListOfDefinitions();
            EditingExistingDefinition = false;           
        }

        private bool EditingExistingDefinition
        {
            set; get;
        }

        private void SetupListOfDefinitions()
        {
            foreach (var definition in _definitionClient.GetDefinitions())
            {
                listBoxDefinitions.Items.Add(definition.Name);
            }
        }

        private string GetUserSelectedFolder()
        {
            var dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                return dialog.SelectedPath;
            }

            return null;
        }

        private void HandleFolderSelection(TextBox targetControlToUpdate)
        {
            var value = GetUserSelectedFolder();

            if (value != null)
            {
                targetControlToUpdate.Text = value;
            }
        }

        private void HandleFolderSelectionMultiple(ListBox targetControlToUpdate)
        {
            var value = GetUserSelectedFolder();

            if (value != null && !targetControlToUpdate.Items.Contains(value))
            {
                targetControlToUpdate.Items.Add(value);
            }
        }

        private void HandleDefinition(Action<DeployDefinition> handleDefinition)
        {
            handleDefinition(CreateDefinitionFromForm());
            ClearForm();
            UpdateDefinitionsList();
        }

        private void HandleNewSelectedDefinition()
        {
            if (listBoxDefinitions.SelectedItem != null)
            {
                var definition = _definitionClient.GetDefinition(listBoxDefinitions.SelectedItem.ToString());

                if (definition != null)
                {
                    FillForm(definition);
                    EditingExistingDefinition = true;
                    txtName.Enabled = false;
                }
            }
        }

        private DeployDefinition CreateDefinitionFromForm()
        {
            var definition = new DeployDefinition()
            {
                Name = txtName.Text,
                SourcePath = txtSource.Text,
                BackupPath = txtBackup.Text
            };

            foreach (var targetLocation in listBoxTargets.Items)
            {
                definition.TargetPaths.Add(targetLocation.ToString());
            }

            return definition;
        }

        private void UpdateDefinitionsList()
        {
            listBoxDefinitions.Items.Clear();

            foreach(var definition in _definitionClient.GetDefinitions())
            {
                listBoxDefinitions.Items.Add(definition.Name);
            }
        }

        private void ClearForm()
        {
            txtBackup.Text = string.Empty;
            txtName.Text = string.Empty;
            txtSource.Text = string.Empty;
            listBoxTargets.Items.Clear();
        }

        private void ClearEditingState()
        {
            EditingExistingDefinition = false;
            txtName.Enabled = true;
            listBoxDefinitions.ClearSelected();
        }

        private void FillForm(DeployDefinition definition)
        {
            txtBackup.Text = definition.BackupPath;
            txtName.Text = definition.Name;
            txtSource.Text = definition.SourcePath;
            listBoxTargets.Items.Clear();

            foreach (var targetLocation in definition.TargetPaths)
            {
                listBoxTargets.Items.Add(targetLocation.ToString());
            }
        }

        private void btnSelectSourceFolder_Click(object sender, EventArgs e)
        {
            HandleFolderSelection(txtSource);
        }

        private void btnSelectBackupFolder_Click(object sender, EventArgs e)
        {
            HandleFolderSelection(txtBackup);
        }

        private void btnSelectTargetFolders_Click(object sender, EventArgs e)
        {
            HandleFolderSelectionMultiple(listBoxTargets);
        }

        private void btnSaveDefinition_Click(object sender, EventArgs e)
        {
            if(EditingExistingDefinition)
            {
                HandleDefinition(_definitionClient.UpdateDefinition);
                ClearEditingState();
            }
            else
            {
                HandleDefinition(_definitionClient.AddDefinition);
            }        
        }

        private void btnCreateNewDefinition_Click(object sender, EventArgs e)
        {
            ClearForm();
            ClearEditingState();
        }

        private void listBoxDefinitions_SelectedIndexChanged(object sender, EventArgs e)
        {
            HandleNewSelectedDefinition();
        }

        private void listBoxTargets_KeyUp(object sender, KeyEventArgs e)
        {
            if (!(e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back))
            {
                return;
            }

            if (listBoxTargets.SelectedItem == null)
            {
                return;
            }

            // Todo: Move strings to resource file
            if (MessageBox.Show("Do you want to remove this target?", "Remove target", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                listBoxTargets.Items.Remove(listBoxTargets.SelectedItem);
            }
        }

        private void btnBackupAndDeploy_Click(object sender, EventArgs e)
        {
            if(listBoxDefinitions.SelectedItem == null)
            {
                return;
            }

            var definition = _definitionClient.GetDefinition(listBoxDefinitions.SelectedItem.ToString());

            foreach(var target in definition.TargetPaths)
            {
                _backupClient.Backup(target, definition.BackupPath);
                _logClient.UpdateRecord(target);
                _deployClient.Deploy(definition.SourcePath, target);
            }                            
        }

        private void listBoxDefinitions_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(allTabs.SelectedIndex == 2)
            {
                return;
            }

            allTabs.SelectedIndex = 2;
            HandleNewSelectedDefinition();
        }

        /// <summary>
        /// TODO: Remove this. Temporary test method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            var definition = _definitionClient.GetDefinition(listBoxDefinitions.SelectedItem.ToString());

            _logClient.UpdateRecordWithSource(definition.SourcePath);

            foreach (var target in definition.TargetPaths)
            {             
                _logClient.UpdateRecord(target);
            }

            _logClient.Log();
        }
    }
}
